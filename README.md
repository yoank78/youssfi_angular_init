# youssfi_angular_init
## Développement Web Front End : Angular Framework
* Part 1 - Angular Framework Concepts de base et Architecture : https://www.youtube.com/watch?v=zT_nQ2oNeRM
* Part 2 - Angular Framework Prise en main : https://www.youtube.com/watch?v=d3eGRvfVq2s
* Part 3 - Angular Framework - Authentification : https://www.youtube.com/watch?v=rE9uJ5DkQ7M
* Part 4 - Angular Framework- Reactive Forms : https://www.youtube.com/watch?v=lGSFLZbN488

## Part 2 - Angular Framework Prise en main
### Install des outils
Ajout de l'extension VSCode : Angular Language Service

vidéo : 00:07:00

Node.js (JavaScript runtime environment)
https://nodejs.org/fr/
$ node --version => v16.18.1

npm (Gestionnaire de package)
https://www.npmjs.com/
$ npm --version => 8.19.2

Angular
https://angular.io/
$ npm install -g @angular/cli
$ ng version => Angular CLI: 14.2.9

## Création projet angular
vidéo : 00:09:00

Initialisation du projet
$ ng new catalog
+ add Angular routing
+ choose stylesheet format -> CSS

vidéo : 00:12:00
Test du projet
$ ng serve

Ressources fournies dans l'application créée par défaut:
* Learn Angular : https://angular.io/tutorial
* CLI Documentation : https://angular.io/cli
* Angular Material : https://material.angular.io/
* Angular Blog : https://blog.angular.io/
* Angular DevTools : https://angular.io/guide/devtools

## projet Angular = projet Node.js
vidéo : 00:20:00

* Fichier package.json => contient la liste des dépendances
"dependencies": {
    "@angular/animations": "^14.2.0",
    ...
}

Ajout librairies :
* Bootstrap
vidéo : 00:21:00
https://getbootstrap.com/
https://www.npmjs.com/package/bootstrap

$ npm install --save bootstrap
L'option --save permet d'ajouter la dépendance dans le fichier package.json (+ package-lock.json)

* Bootstrap-icons
vidéo : 00:22:00
https://icons.getbootstrap.com/
https://www.npmjs.com/package/bootstrap-icons

$ npm install --save bootstrap-icons

## Structure projet Angular
vidéo : 00:23:00

Présentation de la structure d'un projet Angular

vidéo : 00:24:30
Nettoyage des sources fournies avec l'appli par défaut
- app.component.html

## Création de components
vidéo : 00:26:00

A faire à la racine du projet : catalog
$ ng generate component products (ou en raccourci: ng g c products)
CREATE src/app/products/products.component.css (0 bytes)
CREATE src/app/products/products.component.html (23 bytes)
CREATE src/app/products/products.component.spec.ts (613 bytes)
CREATE src/app/products/products.component.ts (283 bytes)
UPDATE src/app/app.module.ts (483 bytes)

$ ng generate component customers (ou en raccourci: ng g c customers)
CREATE src/app/customers/customers.component.css (0 bytes)
CREATE src/app/customers/customers.component.html (24 bytes)
CREATE src/app/customers/customers.component.spec.ts (620 bytes)
CREATE src/app/customers/customers.component.ts (287 bytes)
UPDATE src/app/app.module.ts (577 bytes)

## Modification page d'accueil de l'appli
vidéo : 00:28:00
récupération d'un exemple de navbar bootstrap
https://getbootstrap.com/docs/5.2/components/navbar/

vidéo : 00:29:00
intégration des librairies bootstrap au projet

vidéo : 00:38:30
configuration des routes pour mapper des liens vers des components
modification du fichier : app-routing.module.ts

## Modification component product
vidéo : 00:42:00
vidéo : 00:50:50
-> databinding

## Mise en place de service
vidéo : 00:59:00

$ ng generate service services/product
CREATE src/app/services/product.service.spec.ts (362 bytes)
CREATE src/app/services/product.service.ts (136 bytes)

vidéo : 01:06:30
Passage en asynchrone entre le component et le service pour récupérer la liste des produits
vidéo : 01:14:00
Simulation erreur rencontrée sur l'appel au service pour récupérer les données 

vidéo : 01:17:30
Création de la classe Poduct pour typer les données manipulées

vidéo : 01:31:00
Evolution du mode Product pour ajouter la notion de promotion

vidéo : 01:59:00
Ajout d'un formulaire de recherche
vidéo : 02:01:20
Import module ReactiveFormsModule dans app.module.ts

vidéo : 02:11:30
Gestion de la pagination

vidéo : 02:12:50
Utilisation librairie angular2-uuid pour la génération de UUID
$ npm install angular2-uuid --save

vidéo : 02:31:00
Ajout de la gestion de navigation entre les pages


## Part 3 - Angular Framework - Authentification
https://www.youtube.com/watch?v=rE9uJ5DkQ7M

vidéo : 
@todo: reprendre à partir d'ici



---

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/yoank78/youssfi_angular_init.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/yoank78/youssfi_angular_init/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
